export default function(problem, gradient, xs, limit=100) {
  let count = 0;
  while (!problem.factible(xs) && count < limit) {
    count++;
    xs = gradient(problem, xs);
    let [x1, x2] = xs;
    console.log(count, x1, x2);
  }
  return xs;
}