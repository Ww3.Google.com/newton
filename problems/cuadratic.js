export default class Cuadratic{
  f([x1, x2]) {
    return x1 * x1 + x2 * x2;
  }

  gradient([x1, x2]) {
    return [2 * x1, 2 * x2];
  }

  detHesian([x1, x2]) {
    return 4.0;
  }

  factible([x1, x2]) {
    return false;
  }
}