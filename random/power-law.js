export default function(options){
  let operator, x;
  return (x) => {
    operator = Math.random() < 0.5 ? -1 : 1;
    x = Math.random();
    return operator * Math.pow(1 - x, 1 / (1 - options.alpha));
  }
}